<?php

namespace Drupal\resque\Event;

use Resque_Job;

/**
 * Class BeforeFork.
 *
 * @package Drupal\resque\Event
 * @see https://github.com/resque/php-resque#beforefork
 */
class BeforeFork extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.before.fork';

  /**
   * Worker instance.
   *
   * @var \Resque_Worker
   */
  protected $worker;

  /**
   * The resque job.
   *
   * @var \Resque_Job
   */
  protected $job;

  /**
   * The 'beforeFork' event.
   *
   * Called before php-resque forks to run a job. Argument passed contains the
   * instance of `Resque_Job` for the job about to be run.
   *
   * `beforeFork` is triggered in the **parent** process. Any changes made will
   * be permanent for as long as the **worker** lives.
   *
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Worker::work()
   * @see https://github.com/resque/php-resque#beforefork
   */
  public function __construct(Resque_Job $job) {
    $this->job = $job;
  }

  /**
   * The resque job getter.
   *
   * @return \Resque_Job
   *   The resque job.
   */
  public function getJob(): Resque_Job {
    return $this->job;
  }

}
