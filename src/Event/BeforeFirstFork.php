<?php

namespace Drupal\resque\Event;

use Resque_Worker;

/**
 * Class BeforeFirstFork.
 *
 * @package Drupal\resque\Event
 * @see https://github.com/resque/php-resque#beforefirstfork
 */
class BeforeFirstFork extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.before.first.fork';

  /**
   * Worker instance.
   *
   * @var \Resque_Worker
   */
  protected $worker;

  /**
   * The 'beforeFirstFork' event.
   *
   * Called once, as a worker initializes. Argument passed is the instance of
   * `Resque_Worker` that was just initialized.
   *
   * @param \Resque_Worker $worker
   *   Worker instance.
   *
   * @see \Resque_Worker::startup()
   * @see https://github.com/resque/php-resque#beforefirstfork
   */
  public function __construct(Resque_Worker $worker) {
    $this->worker = $worker;
  }

  /**
   * Worker instance getter.
   *
   * @return \Resque_Worker
   *   Worker instance.
   */
  public function getWorker(): Resque_Worker {
    return $this->worker;
  }

}
