<?php

namespace Drupal\Tests\resque\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the requirements based on no defined workers.
 *
 * @group resque
 */
class NoWorkersRequirementsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['resque'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Test the requirements based on no defined workers.
   */
  public function testRequirements() {
    $user = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($user);
    $this->drupalGet(Url::fromRoute('system.status'));
    $this->assertSession()->pageTextContains('No workers specified.');
    $this->assertSession()->pageTextContains('Enable the contrib or custom module which provides definition of the QueueWorker class or uninstall the "Resque" module.');
  }

}
