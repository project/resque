<?php

namespace Drupal\resque\Event;

/**
 * Class AfterEnqueue.
 *
 * @package Drupal\resque\Event
 * @see https://github.com/resque/php-resque#afterenqueue
 */
class AfterEnqueue extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.after.enqueue';

  /**
   * The name of the class that contains the code to execute the job.
   *
   * @var string
   */
  protected $class;

  /**
   * Any optional arguments that should be passed when the job is executed.
   *
   * @var array
   */
  protected $args;

  /**
   * The name of the queue to place the job in.
   *
   * @var string
   */
  protected $queue;

  /**
   * Process ID of the job.
   *
   * @var string
   */
  protected $id;

  /**
   * AfterEnqueue constructor.
   *
   * Called after a job has been queued using the `Resque::enqueue` method.
   * Arguments passed (in this order) include:
   *
   * - Class - string containing the name of scheduled job
   * - Arguments - array of arguments supplied to the job
   * - Queue - string containing the name of the queue the job was added to
   * - ID - string containing the new token of the enqueued job
   *
   * @param string $class
   *   The name of the class that contains the code to execute the job.
   * @param array $args
   *   Any optional arguments that should be passed when the job is executed.
   * @param string $queue
   *   The name of the queue to place the job in.
   * @param string $id
   *   String containing the new token of the enqueued job.
   *
   * @see \Resque::enqueue()
   * @see https://github.com/resque/php-resque#afterenqueue
   */
  public function __construct(string $class, array $args, string $queue, string $id) {
    $this->class = $class;
    $this->args = $args;
    $this->queue = $queue;
    $this->id = $id;
  }

  /**
   * The class getter.
   *
   * @return string
   *   The class name string.
   */
  public function getClass(): string {
    return $this->class;
  }

  /**
   * The args getter.
   *
   * @return array
   *   The args of the worker.
   */
  public function getArgs(): array {
    return $this->args;
  }

  /**
   * The queue getter.
   *
   * @return string
   *   The queue value.
   */
  public function getQueue(): string {
    return $this->queue;
  }

  /**
   * The id getter.
   *
   * @return string
   *   The id value.
   */
  public function getId(): string {
    return $this->id;
  }

}
