<?php

namespace Drupal\resque;

/**
 * Interface for EventListener.
 *
 * Provides base method and event names provided by the resque library.
 *
 * @package Drupal\resque
 */
interface EventListenerInterface {

  public const EVENT_BEFORE_ENQUEUE = 'beforeEnqueue';
  public const EVENT_AFTER_ENQUEUE = 'afterEnqueue';
  public const EVENT_BEFORE_PERFORM = 'beforePerform';
  public const EVENT_AFTER_PERFORM = 'afterPerform';
  public const EVENT_ON_FAILURE = 'onFailure';
  public const EVENT_BEFORE_FORK = 'beforeFork';
  public const EVENT_AFTER_FORK = 'afterFork';
  public const EVENT_BEFORE_FIRST_FORK = 'beforeFirstFork';
  public const EVENT_AFTER_SCHEDULE = 'afterSchedule';
  public const EVENT_BEFORE_DELAYED_ENQUEUE = 'beforeDelayedEnqueue';

  /**
   * Call this method to define event listeners.
   */
  public function listen(): void;

}
