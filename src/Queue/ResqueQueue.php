<?php

namespace Drupal\resque\Queue;

use Drupal\Core\Queue\QueueInterface;
use Drupal\resque\DrupalWorkerJob;

/**
 * Class Resque.
 *
 * @package Drupal\resque\Queue
 */
class ResqueQueue implements QueueInterface, SchedulerQueueInterface {

  /**
   * Resque constructor.
   *
   * @param string $queue
   *   The queue name.
   */
  public function __construct(string $queue) {
    $this->queue = $queue;
    if (!self::$connectionEstablished) {
      /*
       * ToDo: Inject configuration management for connecting to the redis
       *    service.
       */
      \Resque::setBackend(getenv('REDIS_BACKEND'));
      self::$connectionEstablished = TRUE;
    }
  }

  /**
   * Contains the flag is the connection is established.
   *
   * @var bool
   */
  protected static $connectionEstablished;

  /**
   * The queue name of the current queue instance.
   *
   * @var string
   */
  protected $queue;

  /**
   * {@inheritdoc}
   */
  public function createItem($data) {
    return \Resque::enqueue($this->queue, DrupalWorkerJob::class, ['data' => $data]);
  }

  /**
   * {@inheritdoc}
   */
  public function createItemAt($data, $at): void {
    \ResqueScheduler::enqueueAt($at, $this->queue, DrupalWorkerJob::class, ['data' => $data]);
  }

  /**
   * {@inheritdoc}
   */
  public function createItemIn($data, int $in): void {
    \ResqueScheduler::enqueueIn($in, $this->queue, DrupalWorkerJob::class, ['data' => $data]);
  }

  /**
   * {@inheritdoc}
   */
  public function numberOfItems() {
    return \Resque::size($this->queue);
  }

  /**
   * {@inheritdoc}
   */
  public function claimItem($lease_time = 3600) {
    return \Resque::pop($this->queue);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItem($item) {
    \Resque::dequeue($this->queue, [$item]);
  }

  /**
   * {@inheritdoc}
   */
  public function releaseItem($item) {
    \Resque::push($this->queue, $item);
  }

  /**
   * {@inheritdoc}
   */
  public function createQueue() {
    // No actions required.
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteQueue() {
    \Resque::removeQueue($this->queue);
  }

}
