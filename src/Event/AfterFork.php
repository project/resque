<?php

namespace Drupal\resque\Event;

use Resque_Job;

/**
 * Class AfterFork.
 *
 * @package Drupal\resque\Event
 * @see https://github.com/resque/php-resque#afterfork
 */
class AfterFork extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.after.fork';

  /**
   * The resque job.
   *
   * @var \Resque_Job
   */
  protected $job;

  /**
   * The 'afterFork' event.
   *
   * Called after php-resque forks to run a job (but before the job is run).
   * Argument passed contains the instance of `Resque_Job` for the job about to
   * be run.
   *
   * `afterFork` is triggered in the **child** process after forking out to
   * complete a job. Any changes made will only live as long as the **job** is
   * being processed.
   *
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Worker::perform()
   * @see https://github.com/resque/php-resque#afterfork
   */
  public function __construct(Resque_Job $job) {
    $this->job = $job;
  }

}
