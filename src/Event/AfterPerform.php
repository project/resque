<?php

namespace Drupal\resque\Event;

use Resque_Job;

/**
 * Class AfterPerform.
 *
 * @package Drupal\resque\Event
 * @see https://github.com/resque/php-resque#afterperform
 */
class AfterPerform extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.after.perform';

  /**
   * The resque job.
   *
   * @var \Resque_Job
   */
  protected $job;

  /**
   * The 'afterPerform' event.
   *
   * Called after the `perform` and `tearDown` methods on a job are run.
   * Argument passed contains the instance of `Resque_Job` that was just run.
   *
   * Any exceptions thrown will be treated as if they were thrown in a job,
   * causing the job to be marked as having failed.
   *
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Job::perform()
   * @see https://github.com/resque/php-resque#afterperform
   */
  public function __construct(Resque_Job $job) {
    $this->job = $job;
  }

  /**
   * The resque job getter.
   *
   * @return \Resque_Job
   *   The resque job.
   */
  public function getJob(): Resque_Job {
    return $this->job;
  }

}
