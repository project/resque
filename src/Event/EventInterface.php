<?php

namespace Drupal\resque\Event;

/**
 * Interface EventInterface.
 *
 * @package Drupal\resque\Event
 */
interface EventInterface {

  /**
   * Event name getter.
   *
   * @return string
   *   The name of the event.
   */
  public function getName(): string;

}
