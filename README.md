# Drupal Resque Module

This module makes use of an existing php resque implementation:
https://github.com/resque/php-resque

This module implements the Resque API to be able to use Drupal\Core\Queue\QueueWorkerBase
based QueueWorker instances.

This README is for interested developers.

## Requirements

  * Redis installed locally or use a remote server.
  * This module installed on the project via composer.

## How To Use

To use the features provided by the resque/php-resque library there required 3 main components:

- Existing Drupal QueueWorker defined in the codebase, contributed or custom module;
- Worker instance: running process on the current host, remote server, virtual service or docker service;
- Redis server (remote or local);

### QueueWorker configuration

  Add configuration for `setting.php` file to define Redis queue manager for specific queue worker definition.
  Example:
  ```php
  <?php

  // ...

  // The id of the queue worker. In this case id of the example queue worker instance.
  /* @see \Drupal\resque_queue_example\Plugin\QueueWorker\Resque */
  $queue_worker_id = 'resque_queue_example';
  $settings['queue_service_' . $queue_worker_id] = 'resque.queue.resque';
  ```

### Worker instance
  * Worker instance initialization example - Redis installed locally:
    ```shell script
    REDIS_BACKEND=localhost:6379 QUEUE='*' COUNT=2 RESQUE_DRUPAL_ROOT=$(pwd) APP_INCLUDE=$RESQUE_DRUPAL_ROOT/modules/contrib/resque/resque.app.php ../vendor/bin/resque
    ```
  * Worker instance initialization example - Docker4Drupal:
    ```yaml
    service:
      # ...
      resque-worker-1:
        image: devwithlando/php:7.4-fpm2
        container_name: "${PROJECT_NAME}_resque_worker_1"
        environment:
          PHP_SENDMAIL_PATH: /usr/sbin/sendmail -t -i -S mailhog:1025
          PROJECT_BASE_URL: $PROJECT_BASE_URL
          DB_HOST: $DB_HOST
          DB_USER: $DB_USER
          DB_PASSWORD: $DB_PASSWORD
          DB_NAME: $DB_NAME
          DB_DRIVER: $DB_DRIVER
          DB_PORT: $DB_PORT
          REDIS_BACKEND: redis:6379
          QUEUE: "*"
          COUNT: 2
          DRUPAL_ROOT: /var/www/html/web
          APP_INCLUDE: /var/www/html/web/modules/contrib/resque/resque.app.php
        command: vendor/bin/resque
        volumes:
          - ./:/var/www/html:cached
    ```
    or manual separate process initialization for local development:
    ```shell script
    docker-compose exec php bash -c 'cd web/ && REDIS_BACKEND=redis:6379 QUEUE="*" COUNT=2 DRUPAL_ROOT=$(pwd) APP_INCLUDE=$DRUPAL_ROOT/modules/contrib/resque/resque.app.php ../vendor/bin/resque'
    ```

### Scheduler Worker instance
  Optionally, there can be instantiated the scheduler worker. It is responsible to enqueue the queue item at the specific moment of the time.
  * Scheduler Worker instance initialization example - Redis installed locally:
    ```shell script
    REDIS_BACKEND=localhost:6379 ../vendor/bin/resque-scheduler
    ```
  * Scheduler Worker instance initialization example - Docker4Drupal:
    ```yaml
    service:
      # ...
      resque-scheduler-worker-1:
        image: devwithlando/php:7.4-fpm2
        container_name: "${PROJECT_NAME}_resque_scheduler_worker_1"
        environment:
          PHP_SENDMAIL_PATH: /usr/sbin/sendmail -t -i -S mailhog:1025
          PROJECT_BASE_URL: $PROJECT_BASE_URL
          DB_HOST: $DB_HOST
          DB_USER: $DB_USER
          DB_PASSWORD: $DB_PASSWORD
          DB_NAME: $DB_NAME
          DB_DRIVER: $DB_DRIVER
          DB_PORT: $DB_PORT
          REDIS_BACKEND: redis:6379
        command: vendor/bin/resque-scheduler
        volumes:
          - ./:/var/www/html:cached
    ```
    or manual separate process initialization for local development:
    ```shell script
    docker-compose exec php bash -c 'cd web/ && REDIS_BACKEND=redis:6379 ../vendor/bin/resque-scheduler'
    ```
TBD;
