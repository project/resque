<?php

namespace Drupal\Tests\resque\Functional;

use Drupal\Core\Url;

/**
 * Test requirements with available defined workers.
 *
 * @group resque
 */
class OneWorkerRequirementsTest extends NoWorkersRequirementsTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['resque_queue_example'];

  /**
   * Test requirements with available defined workers.
   */
  public function testRequirements() {
    $user = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($user);
    $this->drupalGet(Url::fromRoute('system.status'));
    $this->assertSession()->pageTextContains('Module requires changes in settings.php file.');
    $this->assertSession()->pageTextContains('There are some QueueWorkers which not configured properly. See details at the Resque Settings page');

    $this->writeSettings([
      'settings' => [
        'queue_service_resque_queue_example' => (object) [
          'value' => 'resque.queue.resque',
          'comment' => 'test',
          'required' => TRUE,
        ],
      ],
    ]);

    $this->drupalGet(Url::fromRoute('system.status'));
    $this->assertSession()->pageTextNotContains('Module requires changes in settings.php file.');
    $this->assertSession()->pageTextNotContains('There are some QueueWorkers which not configured properly. See details at the Resque Settings page');
    $this->assertSession()->pageTextContains('Required configurations in settings.php provided.');
    $this->assertSession()->pageTextContains('All QueueWorker defined configured to work with Resque Queue service. See details at the Resque Settings page');
  }

}
