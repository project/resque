<?php

namespace Drupal\resque\Event;

/**
 * Class BeforeDelayedEnqueue.
 *
 * @package Drupal\resque\Event
 */
class BeforeDelayedEnqueue extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.before.delayed.enqueue';

  /**
   * The name of the queue to place the job in.
   *
   * @var string
   */
  protected $queue;

  /**
   * The name of the class that contains the code to execute the job.
   *
   * @var string
   */
  protected $class;

  /**
   * Any optional arguments that should be passed when the job is executed.
   *
   * @var array
   */
  protected $args;

  /**
   * The 'beforeDelayedEnqueue' event.
   *
   * @param string $queue
   *   The name of the queue to place the job in.
   * @param string $class
   *   The name of the class that contains the code to execute the job.
   * @param array $args
   *   Any optional arguments that should be passed when the job is executed.
   *
   * @see \ResqueScheduler_Worker::enqueueDelayedItemsForTimestamp()
   */
  public function __construct(string $queue, string $class, array $args) {
    $this->queue = $queue;
    $this->class = $class;
    $this->args = $args;
  }

  /**
   * The queue getter.
   *
   * @return string
   *   The queue value.
   */
  public function getQueue(): string {
    return $this->queue;
  }

  /**
   * The class getter.
   *
   * @return string
   *   The class value.
   */
  public function getClass(): string {
    return $this->class;
  }

  /**
   * The args getter.
   *
   * @return array
   *   The args value.
   */
  public function getArgs(): array {
    return $this->args;
  }

}
