<?php

namespace Drupal\resque_queue_example\Plugin\QueueWorker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Resque.
 *
 * @QueueWorker(
 *   id = "resque_queue_example",
 *   title = @Translation("Resque example queue"),
 *   resque = {
 *     "enabled" = true,
 *   }
 * )
 *
 * @package Drupal\resque_queue_example\Plugin\QueueWorker
 */
class Resque extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Example of injected service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Do everything you need with your data.
    if (empty($data)) {
      echo "It works - {$this->time->getRequestTime()}\n";
    }
    else {
      \Drupal::logger('resque_queue_example')
        ->info('resque_queue_example: ' . var_export($data, TRUE));
    }
  }

}
