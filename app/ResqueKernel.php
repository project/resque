<?php

namespace Drupal\resque\Kernel;

use Drupal\Core\DrupalKernel;

/**
 * Drupal kernel with disabled caching.
 */
class ResqueKernel extends DrupalKernel {

  /**
   * {@inheritdoc}
   */
  protected function cacheDrupalContainer(array $container_definition) {
    // We don't need cached container.
    return TRUE;
  }

}
