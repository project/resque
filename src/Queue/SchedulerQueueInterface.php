<?php

namespace Drupal\resque\Queue;

interface SchedulerQueueInterface {

  /**
   * Enqueue a job for execution at a given timestamp.
   *
   * Identical to QueueInterface::createItem, however the second argument is
   * a timestamp (either UNIX timestamp in integer format or an instance of the
   * DateTime class in PHP).
   *
   * @param mixed $data
   *   Any arguments that should be passed when the queue item is executed.
   * @param \DateTime|int $at
   *   Instance of PHP DateTime object or int of UNIX timestamp.
   *
   * @see \ResqueScheduler::enqueueAt()
   */
  public function createItemAt($data, $at): void;

  /**
   * Enqueue a queue item in a given number of seconds from now.
   *
   * Identical to QueueInterface::createItem, however the first argument is the
   * number of seconds before the queue item should be executed.
   *
   * @param mixed $data
   *   Any optional arguments that should be passed when the queue item is
   *   executed.
   * @param int $in
   *   Number of seconds from now when the queue item should be executed.
   *
   * @see \ResqueScheduler::enqueueIn()
   */
  public function createItemIn($data, int $in): void;

}
