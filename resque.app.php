<?php

/**
 * @file
 * Autoload drupal job implementations and init event listeners.
 */

use Drupal\resque\EventListenerManager;
use Drupal\resque\Kernel\ResqueKernel;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

$request = Request::createFromGlobals();
$drupal_root = $request->server->get('DRUPAL_ROOT');
/* @var \Composer\Autoload\ClassLoader $class_loader */
$class_loader = include $drupal_root . '/autoload.php';
$default_site_id = 'default';
$site_id = $request->server->get('DRUPAL_SITE_ID') ?? $default_site_id;
$host = NULL;
$sites_file = $drupal_root . '/sites/sites.php';

// Handle multisite configuration.
if (file_exists($sites_file)) {
  // ToDo: handle only the queues related to the specific sub site.
  if ($site_id !== $default_site_id) {
    $sites = [];
    include_once $sites_file;
    /*
     * ToDo: Support definition of host value through env var with
     *  appropriate validation of the site id.
     */
    $host = array_search($site_id, $sites, TRUE);
    $_SERVER['HTTP_HOST'] = $host;
  }
}
// Resolve host url.
$_SERVER['HTTPS'] = 'on';
$_SERVER['HTTP_USER_AGENT'] = NULL;
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['SERVER_PORT'] = NULL;
$_SERVER['REQUEST_METHOD'] = 'GET';
$_SERVER['REQUEST_URI'] = '/';
$_SERVER['SERVER_SOFTWARE'] = NULL;
$_SERVER['PHP_SELF'] = $_SERVER['REQUEST_URI'] . 'index.php';
$_SERVER['SCRIPT_NAME'] = $_SERVER['PHP_SELF'];
chdir($drupal_root);
$kernel = ResqueKernel::createFromRequest($request, $class_loader, 'prod');
try {
  $kernel->boot();
}
catch (Exception $e) {
  die($e->getMessage());
}
$kernel->preHandle($request);
$container = $kernel->getContainer();
// Setup services which are normally initialized from within stack
// middleware or during the request kernel event.
if (PHP_SAPI !== 'cli') {
  $request->setSession($container->get('session'));
}
$request->attributes->set(RouteObjectInterface::ROUTE_OBJECT, new Route('<none>'));
$request->attributes->set(RouteObjectInterface::ROUTE_NAME, '<none>');
$container->get('request_stack')->push($request);
$container->get('router.request_context')->fromRequest($request);

if (!$container->get('module_handler')->moduleExists('resque')) {
  die("The module 'resque' is not installed yet. Install it first before running this worker instance.\n");
}


$event_listener = new EventListenerManager($kernel, $container->get('event_dispatcher'));
$event_listener->listen();
