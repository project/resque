<?php

namespace Drupal\resque\Event;

use Resque_Job;
use Throwable;

/**
 * Class OnFailure.
 *
 * @package Drupal\resque\Event
 * @see https://github.com/resque/php-resque#onfailure
 */
class OnFailure extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.on.failure';

  /**
   * An exception.
   *
   * @var \Throwable
   */
  protected $e;

  /**
   * The resque job.
   *
   * @var \Resque_Job
   */
  protected $job;

  /**
   * The 'onFailure' event.
   *
   * Called whenever a job fails. Arguments passed (in this order) include:
   *
   * - Exception - The exception that was thrown when the job failed
   * - Resque_Job - The job that failed
   *
   * @param \Throwable $exception
   *   An exception.
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Job::fail()
   * @see https://github.com/resque/php-resque#onfailure
   */
  public function __construct(Throwable $exception, Resque_Job $job) {
    $this->e = $exception;
    $this->job = $job;
  }

  /**
   * Exception getter.
   *
   * @return \Throwable
   *   An exception.
   */
  public function getException():
  Throwable {
    return $this->e;
  }

  /**
   * The resque job getter.
   *
   * @return \Resque_Job
   *   The resque job.
   */
  public function getJob(): Resque_Job {
    return $this->job;
  }

}
