<?php

namespace Drupal\resque;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Database\Database;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\DrupalKernelInterface;
use Drupal\resque\Event\AfterEnqueue;
use Drupal\resque\Event\AfterFork;
use Drupal\resque\Event\AfterPerform;
use Drupal\resque\Event\AfterSchedule;
use Drupal\resque\Event\BeforeDelayedEnqueue;
use Drupal\resque\Event\BeforeEnqueue;
use Drupal\resque\Event\BeforeFirstFork;
use Drupal\resque\Event\BeforeFork;
use Drupal\resque\Event\BeforePerform;
use Drupal\resque\Event\EventInterface;
use Drupal\resque\Event\OnFailure;

/**
 * Provides a Resque Event Listener plugin manager.
 *
 * @package Drupal\resque
 */
class EventListenerManager implements EventListenerInterface {

  use DependencySerializationTrait;

  /**
   * Event dispatcher service.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $dispatcher;

  /**
   * The service container.
   *
   * @var \Psr\Container\ContainerInterface
   */
  protected $container;

  /**
   * Drupal kernel instance.
   *
   * @var \Drupal\Core\DrupalKernelInterface
   */
  protected $kernel;

  /**
   * Drupal 8 backward compatibility flag.
   *
   * @var bool
   */
  private $bc;

  /**
   * EventListenerManager constructor.
   *
   * @param \Drupal\Core\DrupalKernelInterface $kernel
   *   Drupal kernel.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $dispatcher
   *   Event dispatcher instance.
   */
  public function __construct(DrupalKernelInterface $kernel, ContainerAwareEventDispatcher $dispatcher) {
    $this->kernel = $kernel;
    $this->dispatcher = $dispatcher;
    $this->bc = version_compare(\Drupal::VERSION, '9.0.0', '<');
  }

  /**
   * Dispatch the 'beforeFirstFork' event.
   *
   * @param \Resque_Worker $worker
   *   Worker instance.
   *
   * @see \Resque_Worker::startup()
   * @see listen()
   */
  public function beforeFirstFork(\Resque_Worker $worker): void {
    $this->dispatchEvent(new BeforeFirstFork($worker));
    // Close potentially outdated connection.
    Database::closeConnection();
  }

  /**
   * Dispatch the 'afterSchedule' event.
   *
   * @param \DateTime|int $at
   *   Instance of PHP DateTime object or int of UNIX timestamp.
   * @param string $queue
   *   The name of the queue to place the job in.
   * @param string $class
   *   The name of the class that contains the code to execute the job.
   * @param array $args
   *   Any optional arguments that should be passed when the job is executed.
   *
   * @see \ResqueScheduler::enqueueAt()
   * @see listen()
   */
  public function afterSchedule($at, string $queue, string $class, array $args): void {
    $this->dispatchEvent(new AfterSchedule($at, $queue, $class, $args));
  }

  /**
   * Dispatch the 'beforeDelayedEnqueue' event.
   *
   * @param string $queue
   *   The name of the queue to place the job in.
   * @param string $class
   *   The name of the class that contains the code to execute the job.
   * @param array $args
   *   Any optional arguments that should be passed when the job is executed.
   *
   * @see \ResqueScheduler_Worker::enqueueDelayedItemsForTimestamp()
   * @see listen()
   */
  public function beforeDelayedEnqueue(string $queue, string $class, array $args): void {
    $this->dispatchEvent(new BeforeDelayedEnqueue($queue, $class, $args));
  }

  /**
   * Dispatch the 'beforeEnqueue' event.
   *
   * @param string $class
   *   The name of the class that contains the code to execute the job.
   * @param array $args
   *   Any optional arguments that should be passed when the job is executed.
   * @param string $queue
   *   The name of the queue to place the job in.
   * @param string $pid
   *   Process ID of the job.
   *
   * @see \Resque::enqueue()
   * @see listen()
   */
  public function beforeEnqueue(string $class, array $args, string $queue, string $pid): void {
    $this->dispatchEvent(new BeforeEnqueue($class, $args, $queue, $pid));
  }

  /**
   * Dispatch the 'afterEnqueue' event.
   *
   * @param string $class
   *   The name of the class that contains the code to execute the job.
   * @param array $args
   *   Any optional arguments that should be passed when the job is executed.
   * @param string $queue
   *   The name of the queue to place the job in.
   * @param string $id
   *   String containing the new token of the enqueued job.
   *
   * @see \Resque::enqueue()
   * @see listen()
   */
  public function afterEnqueue(string $class, array $args, string $queue, string $id): void {
    $this->dispatchEvent(new AfterEnqueue($class, $args, $queue, $id));
  }

  /**
   * Dispatch the 'beforeFork' event.
   *
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Worker::work()
   * @see listen()
   */
  public function beforeFork(\Resque_Job $job): void {
    $this->dispatchEvent(new BeforeFork($job));
  }

  /**
   * Dispatch the 'afterFork' event.
   *
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Worker::perform()
   * @see listen()
   */
  public function afterFork(\Resque_Job $job): void {
    // As we can change any context without influence to the parent process,
    // @todo: Fix problem consider determining the request time here.
    // @todo: Fix problem consider determining the current site context here.
    // Close potentially outdated connection.
    Database::closeConnection();
    // Reconnect to the default database.
    Database::getConnection();
    $this->dispatchEvent(new AfterFork($job));
    // Rebuild container before executing the job.
    $this->kernel->rebuildContainer();
  }

  /**
   * Dispatch the 'beforePerform' event.
   *
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @throws \Resque_Exception
   * @see \Resque_Job::perform()
   * @see listen()
   */
  public function beforePerform(\Resque_Job $job): void {
    $instance = $job->getInstance();
    if (is_callable([$instance, 'getDependencies'])) {
      // Handle the job dependencies.
      $instance->getDependencies($this->kernel->getContainer());
    }
    $this->dispatchEvent(new BeforePerform($job));
  }

  /**
   * Dispatch the 'onFailure' event.
   *
   * @param \Throwable $exception
   *   An exception.
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Job::fail()
   * @see listen()
   */
  public function onFailure(\Throwable $exception, \Resque_Job $job): void {
    $this->dispatchEvent(new OnFailure($exception, $job));
  }

  /**
   * Dispatch the 'afterPerform' event.
   *
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Job::perform()
   * @see listen()
   */
  public function afterPerform(\Resque_Job $job): void {
    $this->dispatchEvent(new AfterPerform($job));
  }

  /**
   * {@inheritdoc}
   */
  public function listen(): void {
    \Resque_Event::listen(
      static::EVENT_BEFORE_FIRST_FORK,
      [$this, 'beforeFirstFork']
    );
    \Resque_Event::listen(
      static::EVENT_AFTER_SCHEDULE,
      [$this, 'afterSchedule']
    );
    \Resque_Event::listen(
      static::EVENT_BEFORE_DELAYED_ENQUEUE,
      [$this, 'beforeDelayedEnqueue']
    );
    \Resque_Event::listen(
      static::EVENT_BEFORE_ENQUEUE,
      [$this, 'beforeEnqueue']
    );
    \Resque_Event::listen(
      static::EVENT_AFTER_ENQUEUE,
      [$this, 'afterEnqueue']
    );
    \Resque_Event::listen(
      static::EVENT_BEFORE_FORK,
      [$this, 'beforeFork']
    );
    \Resque_Event::listen(
      static::EVENT_AFTER_FORK,
      [$this, 'afterFork']
    );
    \Resque_Event::listen(
      static::EVENT_BEFORE_PERFORM,
      [$this, 'beforePerform']
    );
    \Resque_Event::listen(
      static::EVENT_ON_FAILURE,
      [$this, 'onFailure']
    );
    \Resque_Event::listen(
      static::EVENT_AFTER_PERFORM,
      [$this, 'afterPerform']
    );
  }

  /**
   * Dispatcher backward compatibility helper method.
   *
   * @param \Drupal\resque\Event\EventInterface $event
   *   Event to dispatch.
   */
  public function dispatchEvent(EventInterface $event) {
    if ($this->bc) {
      $this->dispatcher->dispatch($event->getName(), $event);
    }
    else {
      $this->dispatcher->dispatch($event);
    }
  }
}
