<?php

namespace Drupal\resque\Event;

/**
 * Class AfterSchedule.
 *
 * @package Drupal\resque\Event
 */
class AfterSchedule extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.after.schedule';

  /**
   * Instance of PHP DateTime object or int of UNIX timestamp.
   *
   * @var \DateTime|int
   */
  protected $at;

  /**
   * The name of the queue to place the job in.
   *
   * @var string
   */
  protected $queue;

  /**
   * The name of the class that contains the code to execute the job.
   *
   * @var string
   */
  protected $class;

  /**
   * Any optional arguments that should be passed when the job is executed.
   *
   * @var array
   */
  protected $args;

  /**
   * The 'afterSchedule' event.
   *
   * @param \DateTime|int $at
   *   Instance of PHP DateTime object or int of UNIX timestamp.
   * @param string $queue
   *   The name of the queue to place the job in.
   * @param string $class
   *   The name of the class that contains the code to execute the job.
   * @param array $args
   *   Any optional arguments that should be passed when the job is executed.
   *
   * @see \ResqueScheduler::enqueueAt()
   */
  public function __construct($at, string $queue, string $class, array $args) {
    $this->at = $at;
    $this->queue = $queue;
    $this->class = $class;
    $this->args = $args;
  }

  /**
   * The at time value getter.
   *
   * @return \DateTime|int
   *   The at time value.
   */
  public function getAt() {
    return $this->at;
  }

  /**
   * The queue getter.
   *
   * @return string
   *   The queue value.
   */
  public function getQueue(): string {
    return $this->queue;
  }

  /**
   * The class getter.
   *
   * @return string
   *   The class value.
   */
  public function getClass(): string {
    return $this->class;
  }

  /**
   * The args getter.
   *
   * @return array
   *   The args value.
   */
  public function getArgs(): array {
    return $this->args;
  }

}
