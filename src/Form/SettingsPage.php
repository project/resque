<?php

namespace Drupal\resque\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsPage.
 *
 * @package Drupal\resque\Form
 */
class SettingsPage extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  public function __construct(ConfigFactoryInterface $config_factory, QueueWorkerManagerInterface $queue_worker_manager) {
    parent::__construct($config_factory);
    $this->queueWorkerManager = $queue_worker_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.queue_worker')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resque_settings_page';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['base_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      // ToDo: Create granular resque database connectivity configuration.
      'info' => ['#markup' => t('No settings yet')],
    ];
    $queues_section = [
      '#type' => 'details',
      '#title' => $this->t('Detected queues'),
      'i' => [],
    ];
    $not_covered_list = [];
    $prefix = 'queue_service_';
    $resque_queue_service = 'resque.queue.resque';
    foreach ($this->queueWorkerManager->getDefinitions() as $queue_worker_definition) {
      $id = $queue_worker_definition['id'];
      $queue_service = Settings::get($prefix . $id);
      $has_settings = $queue_service === $resque_queue_service;
      if (!isset($queue_worker_definition['resque']) && !$has_settings) {
        $not_covered_list[$id] = $queue_worker_definition['title'];
        continue;
      }

      $status = $has_settings ? $this->t('Configured') : $this->t('Requires configuration');

      $queues_section['i'][$id] = [
        '#type' => 'details',
        '#title' => $status . ': ' . $queue_worker_definition['title'] . ' [' . $id . ']',
        'info' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t(
            'Provider - "%provider"<br />Class - "%class"',
            [
              '%provider' => $queue_worker_definition['provider'],
              '%class' => $queue_worker_definition['class'],
            ]
          ),
        ],
      ];
      if (!$has_settings) {
        $queues_section['i'][$id]['snippet'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t(
            '<b>Make sure that <em>settings.php</em> file contains the following code snippet or equivalent definition:</b><br />%snippet',
            ['%snippet' => "\$settings['{$prefix}{$id}'] = '{$resque_queue_service}';"]
          ),
        ];
      }
    }
    if (!empty($not_covered_list)) {
      array_walk($not_covered_list, function (&$v, $k) {
        $v .= ' [' . $k . ']';
      });
      $queues_section['i'][] = [
        '#type' => 'details',
        '#title' => $this->t('Not processed by Resque module'),
        'list' => [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $not_covered_list,
          '#attributes' => ['class' => 'mylist'],
          '#wrapper_attributes' => ['class' => 'container'],
        ],
      ];
    }
    if (empty($queues_section['i'])) {
      $queues_section['i']['#markup'] = t('No queue workers detected.');
    }
    $worker_section = [
      '#type' => 'details',
      '#title' => $this->t('Worker info'),
      'info' => ['#markup' => t('No settings yet')],
    ];
    $form['info_section'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration info'),
      'queues' => $queues_section,
      'worker' => $worker_section,
    ];
    return $form;
  }

}
