<?php

namespace Drupal\resque\Queue;

/**
 * Class QueueResqueFactory.
 *
 * @package Drupal\resque\Queue
 */
class QueueResqueFactory {

  /**
   * Resque queue manager getter.
   *
   * @param string $name
   *   Name of the queue.
   *
   * @return \Drupal\resque\Queue\ResqueQueue
   *   Queue manager instance.
   */
  public function get(string $name): ResqueQueue {
    return new ResqueQueue($name);
  }

}
