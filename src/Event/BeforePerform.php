<?php

namespace Drupal\resque\Event;

use Resque_Job;

/**
 * Class BeforePerform.
 *
 * @package Drupal\resque\Event
 * @see https://github.com/resque/php-resque#beforeperform
 */
class BeforePerform extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.before.perform';

  /**
   * The resque job.
   *
   * @var \Resque_Job
   */
  protected $job;

  /**
   * The 'beforePerform' event.
   *
   * Called before the `setUp` and `perform` methods on a job are run. Argument
   * passed contains the instance of `Resque_Job` for the job about to be run.
   *
   * You can prevent execution of the job by throwing an exception of
   * `Resque_Job_DontPerform`. Any other exceptions thrown will be treated as
   * if they were thrown in a job, causing the job to fail.
   *
   * @param \Resque_Job $job
   *   The resque job.
   *
   * @see \Resque_Job::perform()
   * @see \Resque_Job_DontPerform
   * @see https://github.com/resque/php-resque#beforeperform
   */
  public function __construct(Resque_Job $job) {
    $this->job = $job;
  }

  /**
   * The job getter.
   *
   * @return \Resque_Job
   *   The job instance.
   */
  public function getJob(): Resque_Job {
    return $this->job;
  }

}
