<?php

namespace Drupal\resque\Event;

/**
 * Class BeforeEnqueue.
 *
 * @package Drupal\resque\Event
 * @see https://github.com/resque/php-resque#beforeenqueue
 */
class BeforeEnqueue extends EventBase {

  /**
   * An event name.
   */
  public const NAME = 'drupal.resque.before.enqueue';

  /**
   * The name of the class that contains the code to execute the job.
   *
   * @var string
   */
  protected $class;

  /**
   * Any optional arguments that should be passed when the job is executed.
   *
   * @var array
   */
  protected $args;

  /**
   * The name of the queue to place the job in.
   *
   * @var string
   */
  protected $queue;

  /**
   * Process ID of the job.
   *
   * @var string
   */
  protected $pid;

  /**
   * The 'beforeFirstFork' event.
   *
   * Called immediately before a job is enqueued using the `Resque::enqueue`
   * method. Arguments passed (in this order) include:
   *
   * - Class - string containing the name of the job to be enqueued
   * - Arguments - array of arguments for the job
   * - Queue - string containing the name of the queue the job is to be
   *   enqueued in
   * - ID - string containing the token of the job to be enqueued
   *
   * You can prevent enqueuing of the job by throwing an exception of
   * `Resque_Job_DontCreate`.
   *
   * @param string $class
   *   The name of the class that contains the code to execute the job.
   * @param array $args
   *   Any optional arguments that should be passed when the job is executed.
   * @param string $queue
   *   The name of the queue to place the job in.
   * @param string $pid
   *   Process ID of the job.
   *
   * @see \Resque::enqueue()
   * @see \Resque_Job_DontCreate
   * @see https://github.com/resque/php-resque#beforeenqueue
   */
  public function __construct(string $class, array $args, string $queue, string $pid) {
    $this->class = $class;
    $this->args = $args;
    $this->queue = $queue;
    $this->pid = $pid;
  }

  /**
   * The class getter.
   *
   * @return string
   *   The class value.
   */
  public function getClass(): string {
    return $this->class;
  }

  /**
   * The args getter.
   *
   * @return array
   *   The args value.
   */
  public function getArgs(): array {
    return $this->args;
  }

  /**
   * The queue getter.
   *
   * @return string
   *   The queue value.
   */
  public function getQueue(): string {
    return $this->queue;
  }

  /**
   * The pid getter.
   *
   * @return string
   *   The pid value.
   */
  public function getPid(): string {
    return $this->pid;
  }

}
