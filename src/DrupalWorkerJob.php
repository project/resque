<?php

namespace Drupal\resque;

use Psr\Container\ContainerInterface;

/**
 * Class DrupalWorkerJob.
 *
 * @property mixed args
 *   The payload of the job.
 * @property string queue
 *   The queue name assigned for the job.
 * @package Drupal\resque
 */
class DrupalWorkerJob {

  /**
   * Queue worker manager instance.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManager|mixed
   */
  protected $queueWorkerManager;

  /**
   * Inject require dependencies to perform queue item using Drupal API.
   *
   * @param \Psr\Container\ContainerInterface $container
   *   Container instance.
   */
  public function getDependencies(ContainerInterface $container): void {
    $this->queueWorkerManager = $container->get('plugin.manager.queue_worker');
  }

  /**
   * Perform queue item.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Exception
   */
  public function perform(): void {
    $this->queueWorkerManager
      // Get instance of the defined queue worker.
      ->createInstance($this->queue)
      // Perform defined `processItem` method.
      ->processItem($this->args['data']);
  }

}
