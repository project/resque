<?php

namespace Drupal\resque\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class EventBase.
 *
 * @package Drupal\resque\Event
 */
abstract class EventBase extends Event implements EventInterface {

  /**
   * The name of the event.
   */
  protected const NAME = 'drupal.resque.event.example.name';

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this::NAME;
  }

}
